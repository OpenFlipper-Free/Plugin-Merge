/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/

#pragma once

#include <QObject>

#include <OpenFlipper/BasePlugin/BaseInterface.hh>
#include <OpenFlipper/BasePlugin/ToolboxInterface.hh>
#include <OpenFlipper/BasePlugin/LoadSaveInterface.hh>
#include <OpenFlipper/BasePlugin/LoggingInterface.hh>
#include <OpenFlipper/BasePlugin/ScriptInterface.hh>

#include "MergeToolbox.hh"

class MergePlugin : public QObject, BaseInterface, ToolboxInterface, LoggingInterface, LoadSaveInterface, ScriptInterface
{
Q_OBJECT
Q_INTERFACES(BaseInterface)
Q_INTERFACES(ToolboxInterface)
Q_INTERFACES(LoggingInterface)
Q_INTERFACES(LoadSaveInterface)
Q_INTERFACES(ScriptInterface)
  Q_PLUGIN_METADATA(IID "org.OpenFlipper.Plugins.Plugin-Merge")



signals:
  //BaseInterface
  void updateView();
  void updatedObject(int _identifier, const UpdateType& _type);

  //LoggingInterface:
  void log( Logtype _type, QString _message );
  void log( QString _message );

  // LoadSaveInterface
  void addEmptyObject( DataType _type, int &_objectId );
  void deleteObject( int _id );
  
  // ToolboxInterface
  void addToolbox( QString _name  , QWidget* _widget );
  void addToolbox( QString _name  , QWidget* _widget , QIcon* _icon );

  // ScriptInterface
  void setSlotDescription(QString     _slotName,   QString     _slotDescription,
                          QStringList _parameters, QStringList _descriptions);

  void cleanup( DataType _type, bool _deleteSeparateObjects);

public :

  MergePlugin();
  ~MergePlugin();

  QString name() { return (QString("Merge")); };
  QString description( ) { return (QString("Merge target objects")); };

private :
  MergeToolBox* tool_;
  QIcon* toolIcon_;
  std::vector<int> convertedIds;
  std::vector< BaseObjectData* > objects;
  int polyMergeID, triMergeID;

  DataType checkType(const std::vector< BaseObjectData* > &);

private slots :
  // Tell system that this plugin runs without ui
  void noguiSupported( ) {} ;

public slots:
  // BaseInterface
  void initializePlugin();
  void pluginsInitialized();
  
  void mergeObjects();

  /**
   * @brief mergeObjects merges multiple OpenFlipper Objects of Type TriMesh or PolyMesh into one combined Mesh.
   * @param _objects a vector of OpenFlipper Objects that shall be merged.
   * @param _name The name of the merged object (default is "merged object")
   * @param _deleteSeparateObjects flag to determine if the separate objects shall be deleted after merging (default is true)
   * @return
   */
  int mergeObjects(const std::vector< BaseObjectData* > & _objects, QString _name = "merged object", bool _deleteSeparateObjects = true, DataType type_ = typeId("TriangleMesh"));

  /**
   * @brief mergeObjects
   * @param _objects
   * @param _name
   * @param _deleteSeparateObjects
   * @param type_
   * @return
   */
  int mergeObjects(IdList _objects, QString _name = "merged object", bool _deleteSeparateObjects = true, DataType type_ = typeId("TriangleMesh"));

  /**
   * @brief slotCleanup is called when the cleanup event is processed at the end of mergeObjects.
   * @param _type the datatype that was used for merging
   * @param _deleteSeparateObjects flag to determine if separated objects shall be deleted.
   */
  void slotCleanup(DataType _type, bool _deleteSeparateObjects);

//template functions
private:

  template< class MeshT >
  void mergeMeshes( const std::vector< MeshT* >& _meshes );

  template< class MeshT >
  void mergeMeshes( const std::vector< MeshT* >& _meshes, typename MeshT::VertexHandle& _vhB);

  public slots:
    QString version() { return QString("1.1"); };

};

