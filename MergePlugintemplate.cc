/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/

#define MERGEPLUGIN_C

#include "MergePlugin.hh"
#include <OpenMesh/Core/Utils/Property.hh>
#include <ObjectTypes/PolyMesh/PolyMesh.hh>
#include <ObjectTypes/TriangleMesh/TriangleMesh.hh>

///merges Meshes into the first mesh
template< class MeshT >
void MergePlugin::mergeMeshes(const std::vector< MeshT* >& _meshes)
{
  typename MeshT::VertexHandle vhB;

  mergeMeshes(_meshes, vhB);
}

///merges meshes together and maps the VertexHandle _vhB from _meshes[max] to its new handle in _meshes[0]
template< class MeshT >
void MergePlugin::mergeMeshes(const std::vector< MeshT* >& _meshes, typename MeshT::VertexHandle& _vhB)
{

  for (uint i=0; i < _meshes.size(); i++)
  if ( _meshes[i] == 0) {
    emit log(LOGERR,"Unable to get Meshes.");
    return;
  }

  typename MeshT::VertexHandle tmp;

  for (uint i=1; i < _meshes.size(); i++){

    // COPY VERTICES into the first mesh
    //

    OpenMesh::VPropHandleT< typename MeshT::VertexHandle > vertexID;
    _meshes[i]->add_property(vertexID, "Vertex ID Property" );

    //iterate over all vertices of the current mesh
    for (auto v_it : _meshes[i]->vertices()){
      //add vertex to _meshes[0] and set vertexID property
      typename MeshT::VertexHandle vh = _meshes[0]->add_vertex( _meshes[i]->point(v_it) );
      _meshes[i]->property(vertexID, v_it) = vh;

      //map vertexHandle _vhB
      if (v_it == _vhB)
        tmp = vh;
    }

    // COPY FACES into the first mesh
    //

    //itertate over all faces of meshes[i]
    for (auto f_it : _meshes[i]->faces()){
      //get corresponding VertexHandles
      std::vector< typename MeshT::VertexHandle > vHandles;

      for (auto fv_it : f_it.vertices())
        vHandles.push_back( _meshes[i]->property(vertexID, fv_it) );

      //add faces to meshA
      _meshes[0]->add_face(vHandles);
    }

    _meshes[i]->remove_property( vertexID );

  }

  _vhB = tmp;
}

template void MergePlugin::mergeMeshes(const std::vector< PolyMesh* >& _meshes);
template void MergePlugin::mergeMeshes(const std::vector< TriMesh* >& _meshes);
